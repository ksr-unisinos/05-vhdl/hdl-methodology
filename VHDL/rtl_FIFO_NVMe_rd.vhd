--------------------------------------------------------------------------------
--
-- Module Name: rtl_FIFO_NVMe_rd
-- Create Date: 09/06/2019 (MM/DD/YYYY)
-- Revision: 0
--
-- Company: ittChip - Unisinos
-- Engineer: Vinícius Gabriel LINDEN
-- 
-- Description: Real-time logic for FIFO NVMe with automatic unload
-- 
--------------------------------------------------------------------------------
-- TODO:

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

library UNISIM;
use UNISIM.VComponents.all;

library work;
use work.NVMe_pkg.all;

entity rtl_FIFO_NVMe_rd is
	generic
	(
		SUBMISSION_CMD 		: boolean := true -- true: submission command; false: completion command
	);
	port
	(
		-- OUTPUT
		valid 				: out std_logic; -- sending new data stream
		ready 				: in std_logic; -- do not send new stream, module is busy

		-- CONTROL
		full 				: in std_logic;
		empty 				: in std_logic;
		unload 				: in std_logic; -- trigger the unloading process
		unloaded 			: out std_logic; -- requested package has been unloaded
		processing 			: out std_logic; -- processing an unload request

		-- FIFO
		counter_en 			: out std_logic; -- internal counter

		-- GENERAL PURPOSE
		error 				: out std_logic;
		rst 				: in std_logic;
		clk 				: in std_logic
	);
end rtl_FIFO_NVMe_rd;

architecture rtl of rtl_FIFO_NVMe_rd is

	constant PACKAGE_LENGTH : integer := nvme_cmd_size(SUBMISSION_CMD);

	type fsm_state is (IDLE, REQUESTED, SENDING);
	signal state_fsm 		: fsm_state;

	signal saturated 		: std_logic;
	signal counter_en_i 	: std_logic;
	signal error_nonstate 	: std_logic; -- undefined state

begin

	error <= error_nonstate;
	counter_en <= counter_en_i;

	COUNTER : entity work.rtl_counter
	generic map
	(
		CNT_RANGE => PACKAGE_LENGTH-1,
		CNT_RESET => 0,
		CNT_SATURATE_EN => false
	)
	port map
	(
		rst => rst,
		clk => clk,
		clk_en => '1',
		cnt_en => counter_en_i,
		cnt => open,
		sat => saturated
	);

	process(rst,clk)
	begin
		if rst = '1' then
			unloaded <= '0';
			valid <= '0';
			state_fsm <= IDLE;
			error_nonstate <= '0';
		elsif clk'event and clk = '1' then
			case state_fsm is
				when IDLE =>
					if unload = '1' and empty = '0' then
						unloaded <= '0';
						valid <= '1';
						if ready = '1' then
							state_fsm <= SENDING;
						else
							state_fsm <= REQUESTED;
						end if;
					end if;
				when REQUESTED =>
					if ready = '1' then
						valid <= '0';
						state_fsm <= SENDING;
					end if;
				when SENDING =>
					valid <= '0';
					if saturated = '1' then
						unloaded <= '1';
						state_fsm <= IDLE;
					end if;
				when others =>
					unloaded <= '0';
					valid <= '0';
					state_fsm <= IDLE;
					error_nonstate <= '1';
			end case;
		end if;
	end process;
	counter_en_i <= '1' when state_fsm = SENDING else '0';
	processing <= '0' when state_fsm = IDLE else '1';

end rtl;


