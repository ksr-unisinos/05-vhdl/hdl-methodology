--------------------------------------------------------------------------------
--
-- Module Name: inst_FIFO
-- Create Date: 06/10/2019 (MM/DD/YYYY)
-- Revision: 2
--
-- Company: ittChip - Unisinos
-- Engineer: Vinícius Gabriel LINDEN
-- 
-- Description: Instantiation for FIFO of lenghts of 8, 16, 32 or 64
-- 
--------------------------------------------------------------------------------
-- TODO:

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

library UNISIM;
use UNISIM.VComponents.all;

library work;
use work.memory_pkg.all;
use work.common_pkg.all;

entity inst_FIFO is
	generic
	(
		FIRST_FALL_THROUGH	: boolean := true; -- First word fall through
		DATA_WIDTH 			: integer := 8; -- FIFO port width (4, 8, 16, 32, 64)
		INDEPENDENT_CLOCK	: boolean := true; -- if true, clock is independent
		SHORT				: boolean := true; -- true: 18Kb; false: 36Kb (for width of 64, this option is useless)
		PROG_EMPTY_THRESH	: integer range 1 to 4096 := 256; -- almost empty threshold
		PROG_FULL_THRESH	: integer range 1 to 4096 := 256 -- almost full threshold
	);
	port
	(
		-- WRITE SIDE
		wrdata 				: in std_logic_vector(DATA_WIDTH-1 downto 0); -- entry data
		wrmeta 				: in std_logic_vector(datawidth2paritywidth(DATA_WIDTH)-1 downto 0); -- entry metadata
		wren 				: in std_logic; -- register new input (each clock cycle)
		wrcount 			: out std_logic_vector(13 downto 0); -- entry counter
		wrrstbusy 			: out std_logic; -- busy with reset mode
		wrrst 				: in std_logic; -- reset
		full 				: out std_logic; -- full flag
		almostfull 			: out std_logic; -- almost full flag (configurable)
		wrerror 			: out std_logic; -- error
		wrclk 				: in std_logic; -- clock

		-- READ SIDE
		rddata 				: out std_logic_vector(DATA_WIDTH-1 downto 0); -- out data
		rdmeta 				: out std_logic_vector(datawidth2paritywidth(DATA_WIDTH)-1 downto 0); -- out metadata
		rden 				: in std_logic; -- read new input (each clock cycle)
		rdcount 			: out std_logic_vector(13 downto 0); -- entry counter
		rdrstbusy 			: out std_logic; -- busy with reset mode
		empty 				: out std_logic; -- empty flag
		almostempty 		: out std_logic; -- almost empty flag (configurable)
		rderror 			: out std_logic; -- error
		rdclk 				: in std_logic -- clock
	);
end inst_FIFO;

architecture inst of inst_FIFO is

	constant PARITY_WIDTH 	: integer := datawidth2paritywidth(DATA_WIDTH);
	constant RD_WR_WIDTH 	: integer := sum_data_parity(DATA_WIDTH);
	signal wrdata_i 		: std_logic_vector(63 downto 0);
	signal wrmeta_i 		: std_logic_vector(7 downto 0);
	signal rddata_i 		: std_logic_vector(63 downto 0);
	signal rdmeta_i 		: std_logic_vector(7 downto 0);

begin

	wrdata_i(DATA_WIDTH-1 downto 0) <= wrdata;
	wrmeta_i(PARITY_WIDTH-1 downto 0) <= wrmeta;
	rddata <= rddata_i(DATA_WIDTH-1 downto 0);
	rdmeta <= rdmeta_i(PARITY_WIDTH-1 downto 0);

	SHORT_INST : if SHORT and DATA_WIDTH /= 64 generate

		wrcount(13) <= '0';
		rdcount(13) <= '0';

		FIFO18E2_inst : entity UNISIM.FIFO18E2
		generic map
		(
			CASCADE_ORDER => "NONE", -- FIRST, LAST, MIDDLE, NONE, PARALLEL
			CLOCK_DOMAINS => indep_clk_to_str(INDEPENDENT_CLOCK), -- COMMON, INDEPENDENT
			FIRST_WORD_FALL_THROUGH => bool2str(FIRST_FALL_THROUGH), -- FALSE, TRUE
			INIT => X"000000000", -- Initial values on output port
			PROG_EMPTY_THRESH => PROG_EMPTY_THRESH, -- Programmable Empty Threshold
			PROG_FULL_THRESH => PROG_FULL_THRESH, -- Programmable Full Threshold
			-- Programmable Inversion Attributes: Specifies the use of the built-in programmable inversion
			IS_RDCLK_INVERTED => '0', -- Optional inversion for RDCLK
			IS_RDEN_INVERTED => '0', -- Optional inversion for RDEN
			IS_RSTREG_INVERTED => '0', -- Optional inversion for RSTREG
			IS_RST_INVERTED => '0', -- Optional inversion for RST
			IS_WRCLK_INVERTED => '0', -- Optional inversion for WRCLK
			IS_WREN_INVERTED => '0', -- Optional inversion for WREN
			RDCOUNT_TYPE => "SIMPLE_DATACOUNT", -- EXTENDED_DATACOUNT, RAW_PNTR, SIMPLE_DATACOUNT, SYNC_PNTR
			READ_WIDTH => RD_WR_WIDTH,
			REGISTER_MODE => "UNREGISTERED", -- DO_PIPELINED, REGISTERED, UNREGISTERED
			RSTREG_PRIORITY => "RSTREG", -- REGCE, RSTREG
			SLEEP_ASYNC => "FALSE", -- FALSE, TRUE
			SRVAL => X"000000000", -- SET/reset value of the FIFO outputs
			WRCOUNT_TYPE => "SIMPLE_DATACOUNT", -- EXTENDED_DATACOUNT, RAW_PNTR, SIMPLE_DATACOUNT, SYNC_PNTR
			WRITE_WIDTH => RD_WR_WIDTH
		)
		port map
		(
			-- Cascade Signals outputs: Multi-FIFO cascade signals
			CASDOUT => open, -- 32-bit output: Data cascade output bus
			CASDOUTP => open, -- 4-bit output: Parity data cascade output bus
			CASNXTEMPTY => open, -- 1-bit output: Cascade next empty
			CASPRVRDEN => open, -- 1-bit output: Cascade previous read enable
			-- Cascade Signals inputs: Multi-FIFO cascade signals
			CASDIN => (others => '0'), -- 32-bit input: Data cascade input bus
			CASDINP => (others => '0'), -- 4-bit input: Parity data cascade input bus
			CASDOMUX => '0', -- 1-bit input: Cascade MUX select
			CASDOMUXEN => '0', -- 1-bit input: Enable for cascade MUX select
			CASNXTRDEN => '0', -- 1-bit input: Cascade next read enable
			CASOREGIMUX => '0', -- 1-bit input: Cascade output MUX select
			CASOREGIMUXEN => '0', -- 1-bit input: Cascade output MUX select enable
			CASPRVEMPTY => '0', -- 1-bit input: Cascade previous empty
			-- Read Data outputs: Read output data
			DOUT => rddata_i(31 downto 0), -- 32-bit output: FIFO data output bus
			DOUTP => rdmeta_i(3 downto 0), -- 4-bit output: FIFO parity output bus.
			-- Status outputs: Flags and other FIFO status outputs
			EMPTY => empty, -- 1-bit output: Empty
			FULL => full, -- 1-bit output: Full
			PROGEMPTY => almostempty, -- 1-bit output: Programmable empty
			PROGFULL => almostfull, -- 1-bit output: Programmable full
			RDCOUNT => rdcount(12 downto 0), -- 13-bit output: Read count
			RDERR => rderror, -- 1-bit output: Read error
			RDRSTBUSY => rdrstbusy, -- 1-bit output: Reset busy (sync to RDCLK)
			WRCOUNT => wrcount(12 downto 0), -- 13-bit output: Write count
			WRERR => wrerror, -- 1-bit output: Write Error
			WRRSTBUSY => wrrstbusy, -- 1-bit output: Reset busy (sync to WRCLK)
			-- Read Control Signals inputs: Read clock, enable and reset input signals
			RDCLK => rdclk, -- 1-bit input: Read clock
			RDEN => rden, -- 1-bit input: Read enable
			REGCE => '0', -- 1-bit input: Output register clock enable
			RSTREG => '0', -- 1-bit input: Output register reset
			SLEEP => '0', -- 1-bit input: Sleep Mode
			-- Write Control Signals inputs: Write clock and enable input signals
			RST => wrrst, -- 1-bit input: Reset
			WRCLK => wrclk, -- 1-bit input: Write clock
			WREN => wren, -- 1-bit input: Write enable
			-- Write Data inputs: Write input data
			DIN => wrdata_i(31 downto 0), -- 32-bit input: FIFO data input bus
			DINP => wrmeta_i(3 downto 0) -- 4-bit input: FIFO parity input bus
		);

	end generate SHORT_INST;

	LONG_INST : if not (SHORT and DATA_WIDTH /= 64) generate

		FIFO36E2_inst : entity UNISIM.FIFO36E2
		generic map
		(
			CASCADE_ORDER => "NONE", -- FIRST, LAST, MIDDLE, NONE, PARALLEL
			CLOCK_DOMAINS => indep_clk_to_str(INDEPENDENT_CLOCK), -- COMMON, INDEPENDENT
			EN_ECC_PIPE => "FALSE", -- ECC pipeline register, (FALSE, TRUE)
			EN_ECC_READ => "FALSE", -- Enable ECC decoder, (FALSE, TRUE)
			EN_ECC_WRITE => "FALSE", -- Enable ECC encoder, (FALSE, TRUE)
			FIRST_WORD_FALL_THROUGH => "FALSE", -- FALSE, TRUE
			INIT => X"000000000000000000", -- Initial values on output port
			PROG_EMPTY_THRESH => PROG_EMPTY_THRESH, -- Programmable Empty Threshold
			PROG_FULL_THRESH => PROG_FULL_THRESH, -- Programmable Full Threshold
			-- Programmable Inversion Attributes: Specifies the use of the built-in programmable inversion
			IS_RDCLK_INVERTED => '0', -- Optional inversion for RDCLK
			IS_RDEN_INVERTED => '0', -- Optional inversion for RDEN
			IS_RSTREG_INVERTED => '0', -- Optional inversion for RSTREG
			IS_RST_INVERTED => '0', -- Optional inversion for RST
			IS_WRCLK_INVERTED => '0', -- Optional inversion for WRCLK
			IS_WREN_INVERTED => '0', -- Optional inversion for WREN
			RDCOUNT_TYPE => "SIMPLE_DATACOUNT", -- EXTENDED_DATACOUNT, RAW_PNTR, SIMPLE_DATACOUNT, SYNC_PNTR
			READ_WIDTH => RD_WR_WIDTH, -- 18-9
			REGISTER_MODE => "UNREGISTERED", -- DO_PIPELINED, REGISTERED, UNREGISTERED
			RSTREG_PRIORITY => "RSTREG", -- REGCE, RSTREG
			SLEEP_ASYNC => "FALSE", -- FALSE, TRUE
			SRVAL => X"000000000000000000", -- SET/reset value of the FIFO outputs
			WRCOUNT_TYPE => "SIMPLE_DATACOUNT", -- EXTENDED_DATACOUNT, RAW_PNTR, SIMPLE_DATACOUNT, SYNC_PNTR
			WRITE_WIDTH => RD_WR_WIDTH -- 18-9
		)
		port map (
			-- Cascade Signals outputs: Multi-FIFO cascade signals
			CASDOUT => open, -- 64-bit output: Data cascade output bus
			CASDOUTP => open, -- 8-bit output: Parity data cascade output bus
			CASNXTEMPTY => open, -- 1-bit output: Cascade next empty
			CASPRVRDEN => open, -- 1-bit output: Cascade previous read enable
			-- Cascade Signals inputs: Multi-FIFO cascade signals
			CASDIN => (others => '0'), -- 64-bit input: Data cascade input bus
			CASDINP => (others => '0'), -- 8-bit input: Parity data cascade input bus
			CASDOMUX => '0', -- 1-bit input: Cascade MUX select input
			CASDOMUXEN => '0', -- 1-bit input: Enable for cascade MUX select
			CASNXTRDEN => '0', -- 1-bit input: Cascade next read enable
			CASOREGIMUX => '0', -- 1-bit input: Cascade output MUX select
			CASOREGIMUXEN => '0', -- 1-bit input: Cascade output MUX select enable
			CASPRVEMPTY => '0', -- 1-bit input: Cascade previous empty
			-- Read Data outputs: Read output data
			DOUT => rddata_i, -- 64-bit output: FIFO data output bus
			DOUTP => rdmeta_i, -- 8-bit output: FIFO parity output bus.
			-- Status outputs: Flags and other FIFO status outputs
			EMPTY => empty, -- 1-bit output: Empty
			FULL => full, -- 1-bit output: Full
			PROGEMPTY => almostempty, -- 1-bit output: Programmable empty
			PROGFULL => almostfull, -- 1-bit output: Programmable full
			RDCOUNT => rdcount, -- 14-bit output: Read count
			RDERR => rderror, -- 1-bit output: Read error
			RDRSTBUSY => rdrstbusy, -- 1-bit output: Reset busy (sync to RDCLK)
			WRCOUNT => wrcount, -- 14-bit output: Write count
			WRERR => wrerror, -- 1-bit output: Write Error
			WRRSTBUSY => wrrstbusy, -- 1-bit output: Reset busy (sync to WRCLK)
			-- Read Control Signals inputs: Read clock, enable and reset input signals
			RDCLK => rdclk, -- 1-bit input: Read clock
			RDEN => rden, -- 1-bit input: Read enable
			REGCE => '0', -- 1-bit input: Output register clock enable
			RSTREG => '0', -- 1-bit input: Output register reset
			SLEEP => '0', -- 1-bit input: Sleep Mode
			-- Write Control Signals inputs: Write clock and enable input signals
			RST => wrrst, -- 1-bit input: Reset
			WRCLK => wrclk, -- 1-bit input: Write clock
			WREN => wren, -- 1-bit input: Write enable
			-- Write Data inputs: Write input data
			DIN => wrdata_i, -- 64-bit input: FIFO data input bus
			DINP => wrmeta_i, -- 8-bit input: FIFO parity input bus
			-- ECC Signals inputs: Error Correction Circuitry ports
			INJECTDBITERR => '0', -- 1-bit input: Inject a double bit error
			INJECTSBITERR => '0', -- 1-bit input: Inject a single bit error
			-- ECC Signals outputs: Error Correction Circuitry ports
			DBITERR => open, -- 1-bit output: Double bit error status
			ECCPARITY => open, -- 8-bit output: Generated error correction parity
			SBITERR => open -- 1-bit output: Single bit error status
		);

	end generate LONG_INST;

end inst;


