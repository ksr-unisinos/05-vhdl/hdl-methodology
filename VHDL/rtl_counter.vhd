--------------------------------------------------------------------------------
--
-- Module Name: rtl_counter
-- Create Date: XX/XX/XXXX (MM/DD/YYYY)
-- Revision: 2
--
-- Company: ittChip - Unisinos
-- Engineer: Rodrigo Marques de Figueiredo & Vinícius Gabriel LINDEN
--
-- Description: General purpose counter
--
--------------------------------------------------------------------------------
-- TODO:

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.NUMERIC_STD.ALL;

library UNISIM;
use UNISIM.VComponents.all;

library work;
use work.log_pkg.all;

entity rtl_counter is
	generic
	(
		CLOCK_EDGE			: std_logic := '1'; -- '1' for rising, '0' for falling
		CNT_RANGE			: natural := 15; -- maximum counter value
		CNT_RESET			: natural := 0; -- reset value
		CNT_SATURATE_EN		: boolean := false -- enables saturation
	);
	port
	(
		cnt					: out std_logic_vector(numbits(CNT_RANGE)-1 downto 0); -- counter value
		clk_en				: in std_logic; -- clock enable
		cnt_en				: in std_logic; -- counter enable
		sat					: out std_logic; -- counter is saturated
		rst					: in std_logic; -- synchronous reset
		clk					: in std_logic
	);
end rtl_counter;

architecture rtl of rtl_counter is

	signal cnt_i		: unsigned(numbits(CNT_RANGE)-1 downto 0) := (others => '0');

begin

	cnt <= std_logic_vector(cnt_i);
	sat <= '1' when cnt_i = CNT_RANGE else '0';

	process(clk)
	begin
		if clk'event and clk = CLOCK_EDGE then
			if clk_en = '1' then

				if cnt_en = '1' then
					if cnt_i < CNT_RANGE then
						cnt_i <= cnt_i + 1;
					elsif cnt_i = CNT_RANGE and not CNT_SATURATE_EN then
						cnt_i <= (others => '0');
					end if;
				end if;

				if rst = '1' then
					cnt_i <= to_unsigned(CNT_RESET,cnt_i'length);
				end if;

			end if;
		end if;
	end process;

end rtl;


