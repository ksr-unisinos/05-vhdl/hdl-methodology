--------------------------------------------------------------------------------
--
-- Module Name: sim_FIFO_NVMe
-- Create Date: 09/06/2019 (MM/DD/YYYY)
--
-- Company: ittChip - Unisinos
-- Engineer: Vinícius Gabriel LINDEN
-- 
-- Description: Stimulae FIFO NVMe
-- 
--------------------------------------------------------------------------------
-- Recommended span of 300 ns for period of 10 ns
-- TODO:


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library UNISIM;
use UNISIM.VComponents.all;

--library STD;
--use STD.TEXTIO.ALL;

library work;
use work.kratus_pkg.all;
use work.NVMe_pkg.all;

entity sim_FIFO_NVMe is
end sim_FIFO_NVMe;


architecture Behavioral of sim_FIFO_NVMe is

	-- DO NOT DELETE
	constant period					: time := 5 ns;
	constant hp						: time := period/2; -- half period

	signal index					: natural := 1; -- index for redability
	signal clk						: std_logic := '1'; -- system base clock

	procedure wait_clk(constant T : in natural) is
	begin
		wait for t*period;
	end procedure;
	-- DELETION RESTRICTION ENDED

	-- UUT
	constant SUBMISSION_CMD 		: boolean := false;
	constant SHORT 					: boolean := true;
	signal stream_wr 				: std_logic_vector(7 downto 0) := (others => '0');
	signal valid_wr 				: std_logic := '0';
	signal ready_wr 				: std_logic;
	signal stream_rd 				: std_logic_vector(7 downto 0);
	signal valid_rd 				: std_logic;
	signal ready_rd 				: std_logic := '0';
	signal full 					: std_logic;
	signal empty 					: std_logic;
	signal block_pkgs 				: std_logic := '0';
	signal unload 					: std_logic := '0';
	signal unloaded 				: std_logic;
	signal processing 				: std_logic;
	signal number_pkgs 				: std_logic_vector(7 downto 0);
	signal error 					: std_logic;
	signal rst 						: std_logic := '1';
	-- END OF UUT

	-- SIMULATION
	constant PACKAGE_LENGTH 		: integer := nvme_cmd_size(SUBMISSION_CMD);
	signal test_case 				: string(1 to 20) := "waiting             ";
-- END OF SIMULATION

begin

	UUT : entity work.rtl_FIFO_NVMe
	generic map
	(
		SUBMISSION_CMD => SUBMISSION_CMD,
		SHORT => SHORT
	)
	port map
	(
		stream_wr => stream_wr,
		valid_wr => valid_wr,
		ready_wr => ready_wr,
		stream_rd => stream_rd,
		valid_rd => valid_rd,
		ready_rd => ready_rd,
		full => full,
		empty => empty,
		block_pkgs => block_pkgs,
		unload => unload,
		unloaded => unloaded,
		processing => processing,
		number_pkgs => number_pkgs,
		error => error,
		rst => rst,
		clk => clk
	);

	-- DO NOT DELETE
	clk <= not clk after hp;
	index <= index + 1 after period;
	-- DELETION RESTRICTION ENDED

	-- SEQUENTIAL DATA GENERATOR
	stream_wr <= std_logic_vector(unsigned(stream_wr)+1) after period;
	-- END OF SEQUENTIAL DATA GENERATOR

	process
	begin
		report "simulation started";
		wait_clk(2);
		rst <= '0';
		wait_clk(10);

		test_case <= "loading pkg 1st     ";
		valid_wr <= '1';
		wait_clk(2);
		valid_wr <= '0';
		wait_clk(PACKAGE_LENGTH);

		test_case <= "clear               ";
		wait_clk(10);

		test_case <= "loading pkg 2nd     ";
		valid_wr <= '1';
		wait_clk(2);
		valid_wr <= '0';
		wait_clk(PACKAGE_LENGTH);

		test_case <= "clear               ";
		wait_clk(10);

		test_case <= "unload 1 pkg instant";
		unload <= '1';
		ready_rd <= '1';
		wait_clk(2);
		ready_rd <= '0';
		unload <= '0';
		wait_clk(PACKAGE_LENGTH);

		test_case <= "clear               ";
		wait_clk(10);

		test_case <= "unload 2 pkg no inst";
		unload <= '1';
		wait_clk(2);
		unload <= '0';
		wait_clk(2);
		ready_rd <= '1';
		wait_clk(2);
		ready_rd <= '0';
		wait_clk(PACKAGE_LENGTH);

		test_case <= "clear               ";
		wait_clk(10);

		test_case <= "write two times     ";
		valid_wr <= '1';
		wait_clk(PACKAGE_LENGTH);
		wait_clk(2);
		valid_wr <= '0';
		wait_clk(PACKAGE_LENGTH);

		test_case <= "clear               ";
		wait_clk(10);

		test_case <= "read while write    ";
		valid_wr <= '1';
		unload <= '1';
		ready_rd <= '1';
		wait_clk(2);
		ready_rd <= '0';
		unload <= '0';
		valid_wr <= '0';
		wait_clk(PACKAGE_LENGTH);

		test_case <= "clear               ";
		wait_clk(10);

		test_case <= "block write         ";
		block_pkgs <= '1';
		valid_wr <= '1';
		wait_clk(2);
		valid_wr <= '0';
		assert ready_wr = '0' report "ready_wr" severity warning;

		test_case <= "clear               ";
		wait_clk(10);

		test_case <= "simulation ended    ";
		report "simulation ended";
		wait;
	end process;

end Behavioral;


