--------------------------------------------------------------------------------
--
-- Module Name: rtl_FIFO_NVMe
-- Create Date: 09/06/2019 (MM/DD/YYYY)
-- Revision: 0
--
-- Company: ittChip - Unisinos
-- Engineer: Vinícius Gabriel LINDEN
-- 
-- Description: Real-time logic FIFO NVMe with automatic load and unload
--			  specifically for NVMe packages
-- 
--------------------------------------------------------------------------------
-- TODO:

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library UNISIM;
use UNISIM.VComponents.all;

library work;
use work.memory_pkg.all;
use work.NVMe_pkg.all;

entity rtl_FIFO_NVMe is
	generic
	(
		SUBMISSION_CMD 		: boolean := true; -- true: submission command; false: completion command
		SHORT				: boolean := true -- true: 32 SC; false: 64 SC
	);
	port
	(
		-- INPUT
		stream_wr			: in std_logic_vector(7 downto 0);
		valid_wr			: in std_logic; -- sending new data stream
		ready_wr			: out std_logic; -- do not send new stream, module is busy

		-- OUTPUT
		stream_rd			: out std_logic_vector(7 downto 0);
		valid_rd			: out std_logic; -- sending new data stream
		ready_rd			: in std_logic; -- do not send new stream, module is busy

		-- CONTROL
		full				: out std_logic;
		empty				: out std_logic;
		block_pkgs 			: in std_logic; -- stop receiving packets
		unload				: in std_logic; -- trigger the unloading process
		unloaded			: out std_logic; -- requested package has been unloaded
		processing			: out std_logic; -- processing an unload request
		number_pkgs			: out std_logic_vector(7 downto 0); -- number of packages stored

		-- GENERAL PURPOSE
		error				: out std_logic;
		rst					: in std_logic;
		clk					: in std_logic
	);
end rtl_FIFO_NVMe;

architecture rtl of rtl_FIFO_NVMe is

	constant PACKAGE_LENGTH	: integer := nvme_cmd_size(SUBMISSION_CMD);
	constant PACKAGES_HOLD 	: integer := FIFO_length(8,SHORT,false)/PACKAGE_LENGTH; -- number of packages the FIFO can hold

	signal wrdata			: std_logic_vector(7 downto 0);
	signal full_i			: std_logic;
	signal empty_i			: std_logic;
	signal wrcount			: std_logic_vector(13 downto 0);
	signal cnt_en_wr		: std_logic;
	signal cnt_en_rd		: std_logic;
	signal wrerror			: std_logic;
	signal rderror			: std_logic;
	signal error_input		: std_logic;
	signal error_output		: std_logic;

begin

	error <= error_input or error_input or wrerror or rderror;

	FIFO : entity work.inst_FIFO
	generic map
	(
		FIRST_FALL_THROUGH => true,
		INDEPENDENT_CLOCK => false,
		DATA_WIDTH => 8,
		SHORT => SHORT,
		PROG_EMPTY_THRESH => PACKAGE_LENGTH*1,
		PROG_FULL_THRESH => PACKAGES_HOLD*PACKAGE_LENGTH
	)
	port map
	(
		wrdata => wrdata,
		wrmeta => (others => '0'),
		wren => cnt_en_wr,
		wrcount => wrcount,
		wrrstbusy => open,
		wrrst => rst,
		wrerror => wrerror,
		wrclk => clk,
		rddata => stream_rd,
		rdmeta => open,
		rden => cnt_en_rd,
		rdcount => open,
		rdrstbusy => open,
		rderror => rderror,
		rdclk => clk,
		empty => empty_i,
		almostempty => open,
		full => open, -- can't use this one, because it is in terms of byte, not cmds!
		almostfull => full_i
	);

	full <= full_i;
	empty <= empty_i;

	WR2FIFO : entity work.rtl_FIFO_NVMe_wr
	generic map
	(
		SUBMISSION_CMD => SUBMISSION_CMD
	)
	port map
	(
		valid => valid_wr,
		ready => ready_wr,
		full => full_i,
		block_pkgs => block_pkgs,
		number_pkgs => number_pkgs,
		counter_en => cnt_en_wr,
		wrcount => wrcount,
		error => error_input,
		rst => rst,
		clk => clk
	);

	FIFO2RD : entity work.rtl_FIFO_NVMe_rd
	generic map
	(
		SUBMISSION_CMD => SUBMISSION_CMD
	)
	port map
	(
		valid => valid_rd,
		ready => ready_rd,
		full => full_i,
		empty => empty_i,
		unload => unload,
		unloaded => unloaded,
		processing => processing,
		counter_en => cnt_en_rd,
		error => error_output,
		rst => rst,
		clk => clk
	);

	DELAY_STREAM : process(clk)
	begin
		if clk'event and clk = '1' then
			wrdata <= stream_wr;
		end if;
	end process DELAY_STREAM;

end rtl;


