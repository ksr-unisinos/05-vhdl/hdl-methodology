--------------------------------------------------------------------------------
--
-- Module Name: memory_pkg
-- Create Date: 16/03/2020 (MM/DD/YYYY)
--
-- Company: ittChip - Unisinos
-- Engineer: Vinícius Gabriel LINDEN
-- 
-- Description: Common functions for FIFO and memory related stuff
-- 
--------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

package memory_pkg is

	-- FUNCTION: datawidth2paritywidth
	-- Takes the data width and translates to the associated parity for BRAM and FIFO instantiations
	function datawidth2paritywidth (data_width : integer) return integer;

	-- FUNCTION: FIFO_length
	-- Outputs the number of entries, based on: port width (w);
	-- FIFO18E2 or FIFO36E2 instantiation (short); and
	-- independent clock (ind_clk)
	function FIFO_length (w         : integer;
						  short     : boolean;
						  ind_clk   : boolean) return integer;

	-- FUNCTION: BRAM_width
	-- Takes the data width and the instatiation type and outputs the address length
	-- w: data width (8, 16, 32, 64) [bit]
	-- short: true, RAMB18E2; false, RAMB36E2
	function BRAM_width (w      : integer;
						 short  : boolean) return integer;

	-- FUNCTION: indep_clk_to_str
	-- Translate boolean to independent clock in FIFO
	function indep_clk_to_str (bool : boolean) return string;

	-- FUNCTION: sum_data_parity
	-- Sums the data and parity bits
	function sum_data_parity (data_width : integer) return integer;

	-- FUNCTION: saturate_at_32
	-- saturates at value 32, useful when dealing with
	-- BRAM primitive
	function saturate_at_32 (data_width : integer) return integer;

	-- FUNCTION: address_low
	-- lower bit of address, given the data width
	-- useful when dealing with BRAM primitive
	function address_low (data_width : integer) return integer;

end memory_pkg;

package body memory_pkg is

	-- FUNCTION: datawidth2paritywidth
	function datawidth2paritywidth (data_width : integer) return integer is
		variable res : integer;
	begin
		case data_width is
			when 4 =>
				res := 1; -- no parity
			when 8 =>
				res := 1;
			when 16 =>
				res := 2;
			when 32 =>
				res := 4;
			when 64 =>
				res := 8;
			when others =>
				res := 1;
		end case;
		return res;
	end function datawidth2paritywidth;

	-- FUNCTION: FIFO_length
	function FIFO_length (w : integer; -- real port width
	short : boolean; -- short (FIFO18E2) instantiation
	ind_clk : boolean) return integer is
		variable res : integer; -- value when in independent clock mode
	begin
		if short and w /= 64 then
			case w is
				when 4 =>
					res := 4095;
				when 8 =>
					res := 2047;
				when 16 =>
					res := 1023;
				when 32 =>
					res := 511;
				when others =>
					res := 0;
			end case;
		else
			case w is
				when 4 =>
					res := 8191;
				when 8 =>
					res := 4095;
				when 16 =>
					res := 2047;
				when 32 =>
					res := 1023;
				when 64 =>
					res := 511;
				when others =>
					res := 0;
			end case;
		end if;
		if ind_clk then
			return res;
		else
			return res + 1;
		end if;
	end function FIFO_length;

	-- FUNCTION: BRAM_width
	function BRAM_width (w: integer;
	short : boolean) return integer is
		variable res : integer;
	begin
		if (not short) or w = 32 or w = 64 then
			case w is
				when 8 =>
					res := 12;
				when 16 =>
					res := 11;
				when 32 =>
					res := 10;
				when 64 =>
					res := 10;
				when others =>
					res := 10;
			end case;
		else
			case w is
				when 8 =>
					res := 11;
				when 16 =>
					res := 10;
				when others =>
					res := 10;
			end case;
		end if;
		return res;
	end function BRAM_width;

	-- FUNCTION: indep_clk_to_str
	function indep_clk_to_str (bool : boolean) return string is
	begin
		if bool then
			return "INDEPENDENT";
		else
			return "COMMON";
		end if;
	end function indep_clk_to_str;

	-- FUNCTION: sum_data_parity
	function sum_data_parity (data_width : integer) return integer is
	begin
		if data_width /= 4 then
			return data_width+datawidth2paritywidth(data_width);
		else
			return data_width;
		end if;
	end function sum_data_parity;

	-- FUNCTION: saturate_at_32
	function saturate_at_32 (data_width : integer) return integer is
	begin
		if data_width <= 32 then
			return data_width;
		else
			return 32;
		end if;
	end function saturate_at_32;

	-- FUNCTION: address_low
	function address_low (data_width : integer) return integer is
		variable res : integer;
	begin
		case data_width is
			when 8 =>
				res := 3;
			when 16 =>
				res := 4;
			when 32 =>
				res := 5;
			when 64 =>
				res := 5;
			when others =>
				res := 1;
		end case;
		return res;
	end function address_low;

end memory_pkg;
